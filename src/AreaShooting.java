import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class AreaShooting {
    static String[][] area = new String[6][6];

    public static void main(String[] args) {
//        Створюємо Scanner один раз для всього додатку
        Scanner sc = new Scanner(System.in);
//        Рандомним чином генеруємо комірку в матриці, попавши
//        по якій користувач виграє гру
        int[] aimCoordinates = generateAim();
        System.out.printf("\nGenerated aim (for testing purposes): %s", Arrays.toString(aimCoordinates));

        System.out.println("\nAll Set. Get ready to rumble!");
//        Ініціалізуємо матрицю та виводимо її в термінал
        initializeArea(area);
//        Створюємо список з двух елементів, який репрезентує координати
//        куди хоче стріляти користувач
        int[] userCoordinates = new int[2];
        
        while(true) {
            System.out.println("\nPlease choose a row to shoot (from 1 to 5)...");
//            Рядок, по якому користувач хоче стріляти
            int userRowNumber = getUserInput(sc);
            userCoordinates[0] = userRowNumber;

            System.out.println("Please choose a column to shoot (from 1 to 5)...");
//            Стовпець, по якому користувач хоче стріляти
            int userColumnNumber = getUserInput(sc);
            userCoordinates[1] = userColumnNumber;

//            Логіка, якщо координати користувача збігаються з
//            ціллю, по якій потрібно попасти
            if (Arrays.equals(aimCoordinates, userCoordinates)) {
                System.out.println("\n\t YOU HAVE WON!");
                area[userCoordinates[0]][userCoordinates[1]] = "X";
                displayArea(area);
                break;
            }

            area[userCoordinates[0]][userCoordinates[1]] = "*";
//            Показуємо оновлене ігрове поле після пострілу
            displayArea(area);
        }

        sc.close();
    }

    static int[] generateAim(){
        Random rand = new Random();
//        Генеруємо числа в межах від 1 до 5 включно
        int randomNumberX = rand.nextInt(5) + 1;
        int randomNumberY = rand.nextInt(5) + 1;
        int[] aimInArea = new int [2];
        aimInArea[0] = randomNumberX;
        aimInArea[1] = randomNumberY;

        return aimInArea;
    }

     static void initializeArea(String[][] area){
//        Робимо розмітку матриці: нумеруємо рядки та стовпці
         for (int x=0; x<area.length; x++){
             area[0][x] = String.valueOf(x);
             area[x][0] = String.valueOf(x);
         }
// Усі інші елементи матриці повинні бути "—"
         for (int x=1; x<area.length; x++) {
             for(int y=1; y<area[x].length; y++) {
                area[x][y] = "—";
             }
         }
// Виводимо матрицю на екран
         displayArea(area);
    }

    static void displayArea(String[][] area){
        for (int x=0; x<area.length; x++){
            for (int y=0; y<area[x].length; y++) {
                System.out.printf("%s | ", area[x][y]);
            }
            System.out.println();
        }
    }

    static int getUserInput(Scanner sc){
        int userNumber = 0;
        while(true){
            try{
                userNumber = sc.nextInt();
                if(userNumber < 1 || userNumber > 5){
                    throw new IllegalArgumentException("Number should be in a range between 1 and 5! Please enter an appropriate number...");
                }
                break;
            }
            catch(InputMismatchException ex) {
                sc.nextLine();
                System.out.println("You entered a string, enter a number please...");
            }
            catch (IllegalArgumentException ex) {
                sc.nextLine();
                System.out.println(ex.getMessage());
            }
        }
        return userNumber;
    }
}